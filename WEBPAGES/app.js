const express = require('express');
const morgan = require('morgan');
const path = require('path');

const app = express();

// Configuração do logger
app.use(morgan('dev'));

// Configuração do EJS como view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Servir arquivos estáticos
app.use(express.static(path.join(__dirname, 'public')));

// Rotas
app.get('/', (req, res) => {
    res.render('home', { title: 'Home' });
});

app.get('/objetivo', (req, res) => {
    res.render('objetivo', { title: 'Objetivo' });
});

app.get('/desenvolvimento', (req, res) => {
    res.render('desenvolvimento', { title: 'Desenvolvimento' });
});

app.get('/conclusao', (req, res) => {
    res.render('conclusao', { title: 'Conclusão' });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Servidor rodando na porta ${PORT}`);
});
